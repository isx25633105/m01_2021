#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# Descripcio: 2. Mostar els arguments rebuts línia a línia, tot numerànt-los.
# ------------------------------
ERR_ARG=1
if [ $# -n 1 ]
then
  echo "ERROR: n* arguments incorrecte"
  echo "USAGE: $0 arg"
  exit $ERR_ARG  
fi

num=1
MAX=$1
while [ $num -le $MAX ]
do
  echo "$num"
  ((num++))
done
exit 0
