#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# Descripcio: Mostrar l̉entrada estàndard numerant línia a línia
# ------------------------------

num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0
