#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# Descripcio: Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar. El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
# ------------------------------
ERR_ARG=1
if [ $# -ne 1 ]
then
  echo "ERROR: n* arguments incorrecte"
  echo "USAGE: $0 arg"
  exit $ERR_ARG  
fi
num=1
MAX=$1
while read -r line
do
 if [ "$num" -lt $MAX ]
 then
    echo "$num:$line"
    ((num++))
 else
    echo "$num:$line"
    exit 0
 fi
done
