#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# Processar línia a línia l̉entrada estàndard, si la línia té més de 60 caràcters la mostra, si no no.
# -----------------------------
num=0
while read -r line
do
  num=$(echo "$line" |  wc -c)
  if  [ $num -gt 60 ]
  then
    echo "$line"	 
  fi  
done
exit 0
