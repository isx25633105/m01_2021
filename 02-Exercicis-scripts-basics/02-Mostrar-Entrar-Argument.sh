#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# Descripcio: 2. Mostar els arguments rebuts línia a línia, tot numerànt-los.
# ------------------------------
ERR_ARG=1
if [ $# -lt 1 ]
then
  echo "ERROR: n* arguments incorrecte"
  echo "USAGE: $0 arg[...]"
  exit $ERR_ARG  
fi

num=1
for linia in $*
do
  echo "$num: $linia"
  ((num++))
done
exit 0
