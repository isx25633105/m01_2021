#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# Mostrar línia a línia l̉entrada estàndard, retallant només els primers 50 caràcters.
# -----------------------------

while read -r line
do
  echo "$line" | cut -c50
done
exit 0
