#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# 
# $ prog dies[...]
# Fer un programa que rep com a arguments noms de dies de la setmana i mostra  quants dies eren laborables i quants festius. Si l̉argument no és un dia de la setmana genera un error per stderr.
# Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday
# ------------------------------
ERR_NARGS=1
ERR_NODIA=2
# 1) Validar argument:
if [ $# -lt 1 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 mes[...]"
  exit $ERR_NARGS
fi
OK=0
num_Laborable=0
num_Festiu=0
# XIXA
for dia in $*
do
  case $dia in
    "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
      ((num_Laborable++));;
    "dissabte"|"diumenge")
      ((num_Festiu++));;
    *)
      echo "$dia no es un dia de la setmana" >> /dev/stderr
      OK=$ERR_NODIA;;
  esac
done

echo "Hi han $num_Laborable dies laborables"
echo "Hi han $num_Festiu dies festius"
exit $OK


