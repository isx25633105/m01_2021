#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# Fer un programa que rep com a argument noms de usuaris, si existeixen en el sistema (en el fitxer /etc/passwd) mostra el usuari per stdout. Si no existeix el mostra per stderr.
# -----------------------------

while read -r line
do
  grep "^$line:" /etc/passwd >> /dev/null
  if [ $? -eq 0 ]
  then 
     echo "el usuari $line existeix"
  else
     echo "el usuari $line no existeix" >> /dev/stderr
  fi
done
exit 0
