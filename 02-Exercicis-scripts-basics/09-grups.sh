#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# Fer un programa que rep com a argument noms de group, si existeixen en el sistema (en el fitxer /etc/group) mostra el nom per stdout. Si no existeix el mostra per stderr.
# -----------------------------

while read -r line
do
  grep "^$line:" /etc/group >> /dev/null
  if [ $? -eq 0 ]
  then 
     echo "el group $line existeix"
  else
     echo "el grup $line no existeix" >> /dev/stderr
  fi
done
exit 0
