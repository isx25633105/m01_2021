#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# 
# $ prog dir
# validar 2 arguments
# copiar el fitxer al directori si existeix
# ------------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOFILE=3
# 1) Validar argument:
if [ $# -lt 2 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 file dir"
  exit $ERR_NARGS
fi

# Validar si es un directori i si es un fitxer
echo "Hi han $# arguments"
echo "la llista: $*" 
echo "$*" | sed 's/^.* //g'
echo "$*" | sed 's/ [^ ]*$//g'
echo "$*" | cut -d' ' -f$#
echo "$*" | cut -d' ' -f1-$(($#-1))



# XIXA
#cp $myfile  $mydir
exit $ok

