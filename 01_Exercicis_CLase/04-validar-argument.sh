# !/bin/bash
#isx25633105 ASIX-M01
#Febrer 2021
#--------------------
# Validar prog te exactament dos arg is els mostra.
# Si no dos args mostrar error, usage i exit error.
# 1) Validem arguments
if [ $# -ne 2 ]
then
  echo "Error: numero arguments incorrecte"
  echo "Usage: $0 ar1 ar2"
  exit 1
fi

# 2) Xixa
echo "Els arguments són: $1, $2"

exit 0
