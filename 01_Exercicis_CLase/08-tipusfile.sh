#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# Mirar si es un dir un link o un regular file
# $ prog file
# ------------------------------
ERR_NARGS=1
ERR_NOEXIST=2
# 1) Validar argument:
if [ $# -ne 1 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

validar=$1

#2) XIXA
if ! [ -e $validar ]; then
	echo "Error: $validar no existeix"
	exit $ERR_NOEXIST
elif [ -h $validar ]; then
	echo "$validar Es un link simple"

elif [ -d $validar ]; then
	echo "$validar es un directori"
elif [ -f $validar ]; then
	echo "$validar es un fitxer regular"
else 
	echo "$validar es una cosa"
fi 
exit 0

