#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# 
# $ prog notes[...]
# ------------------------------
ERR_NARGS=1
ERR_NORANG=2
# 1) Validar argument:
if [ $# -lt 1 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 notas[...]"
  exit $ERR_NARGS
fi
ok=0

# XIXA
for nota in $*
do
  if ! [ $nota -ge 0 -a $nota -le  10 ]
  then
    echo "ERROR: #arg incorrecte" >> /dev/stderr
    echo "usage: $0 mes (valor de [0,10])" >> /dev/stderr
    ok=$ERR_NORANG
  
  elif [ $nota -lt 5 ]
  then
      echo "$nota Suspès"
  elif [ $nota -lt 7 ]
  then
      echo "$nota Aprovat"
  elif [ $nota -lt 9 ]
  then
      echo "$nota Notable"
  else
      echo "$nota Exelent"
  fi
done
exit $ok


