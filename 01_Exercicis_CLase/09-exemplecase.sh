#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# Exemples case
# --------------------------------

case $1 in
  "dll"|"dt"|"dc"|"dj"|"dv")
    echo "és laborable";;
  "ds"|"dm")
    echo "és festiu";;
  *)
    echo "no és un dia de la setmana";;
esac

exit 0

# 2) exemple vocals
case $1 in 
  [aeiou])
    echo "és una vocal";;
  [bcdfghjklmnpqrstvmxyz])
    echo "és una consonant";;
  *)
    echo "és una altra cosa";;
esac

exit 0
    



exit 0

# 1) exemple noms case
case $1 in
 "pere"|"pau"|"joan")
    echo "és un nen";;
"marta"|"anna"|"julia")
    echo "és una nena";;
*)
    echo "indefinit";;
esac
exit 0
