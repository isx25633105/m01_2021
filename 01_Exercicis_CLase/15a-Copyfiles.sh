#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# 
# $ prog dir
# validar 2 arguments
# copiar el fitxer al directori si existeix
# ------------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOFILE=3
# 1) Validar argument:
if [ $# -ne 2 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 file dir"
  exit $ERR_NARGS
fi

# Validar si es un directori i si es un fitxer
myfile=$1
mydir=$2
if ! [ -d $mydir ]
then
  echo "ERROR: $mydir no és un directori"
  echo "usage: $0 file dir"
  exit $ERR_NODIR
fi
if ! [ -e $myfile ]
then
  echo "ERROR: $myfile no és un fitxer"
  echo "usage: $0 file dir"
  exit $ERR_NOFILE
fi
ok=0

# XIXA
cp $myfile  $mydir
exit $ok

