#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# 
# Descripcio; exemples bucle while
# ------------------------------

# while [ cond ]
# do
#  accions
# done

# 9) Numerada i amb maj
num=1

while read -r line
do
  echo "$num: $line" | tr "[a-z]" "[A-Z]"
  num=$((num+1))
done
exit 0

# 8) numerar stdin linia linia
# fins a token Fi
num=1
read -r line
while [ "$line" != "FI" ]
do
  echo "$num: $line"
  num=$((num+1))
  read -r line
done
exit 0

# 7) exemple basic de procesar l'entrada estandar linea a linea
num=1
while read -r line
do 
  echo "$num: $line"
  num=$((num+1))
done
exit 0
# 6) mostrar els arguments
num=1
while [ $# -ge 1 ]
do
  echo "$num: $1, $#,  $*"
  num=$((num+1))
  shift
done
exit 0


# 5) mostrar els arguments
num=1
while [ -n "$1" ]
do
  echo "$num: $1, $#,  $*"
  num=$((num+1))
  shift
done
exit 0


# 4) Mostrar els arguments 
while [ -n "$1" ]
do
  echo "$1 $#:  $*"
  shift
done
exit 0

# 3) mostrar els arguments
while [ $# -gt 0 ]
do
  echo "$#:  $*"
  shift
done
exit 0

# 2) mostrar de n a 0 countdown
MIN=0
num=$1
while [ $num -ge $MIN  ]
do
  echo $num
  ((num--))
done
exit 0
# 1) mostrar numeros del 1 al 10

MAX=10
num=1
while [ $num -le $MAX ]
do
  echo $num
  ((num++))
done
exit 0

