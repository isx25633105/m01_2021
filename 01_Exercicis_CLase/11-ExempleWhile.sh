#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# Descripcio: exemples bucle for
# ------------------------------

# for var in llista-de-valors
#do
#   accions
#done
# $* ·@ es diferencien que "$@" s'expandeix igualment



# 6) llistar numerats tots els logins
llista=$( cut -d: -f1 /etc/passwd )
contador=1
for arg in $llista
do
  echo "$contador: $arg"
  ((contador++))
done
exit 0






# 5) llistat de noms del directori actiu numerat linia a linia
llista_noms=$(ls)
contador=1
for elem in $llista_noms
do
   echo "$contador: $elem"
   ((contador++))
done
exit 0




# 4) llistar els arguments numerats
llista_noms=$(ls)
$contador= 1

for elem in $llista_noms
do
   echo "$contador: $elem"
   contador=$(($contador+1))
done
exit 0

# 3) Iterar per la llista de noms de fitxers que genera ls
llista_noms=$(ls)

for elem in $llista_noms
do
  echo $elem
done
exit 0


# 2b) iterar per cada argument rebut
for arg in "$@"
do
  echo $arg
done
exit 0

# 2) iterar per cada argument rebut
for arg in $*
do
  echo $arg
done
exit 0

# 1b) Iterar una llista de noms
for nom in "pere marta anna ramón"
do
	  echo $nom
done
exit 0

# 1) Iterar una llista de noms
for nom in "pere" "marta" "anna" "ramón"
do 
  echo $nom
done
exit 0

