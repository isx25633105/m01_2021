#! /bin/bash
# isx25633105 ASIX M01-ISO
# Febrer 2021
# Exemple if
# $ prog edat
# ----------------------------------
# 1) Validem arguments
if [ $# -ne 1 ]
then
  echo "Error: #arguments incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi


# Xixa
edat=$1
if [ $edat -ge 18 ]
then
  echo "Es major d'edat te $edat"
fi

exit 0
