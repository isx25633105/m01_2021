#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# 
# $ prog noudir[...]
# validar 1 o mes arguments
# 0 ok tots, 1 eng arg, 2 algun error en crear
# ------------------------------
ERR_NARGS=1
ERR_NODIR=2
status=0
# 1) Validar argument:
if [ $# -lt 1 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# Validar si es un directori
for dir in $*
do
  mkdir -p $dir &> /dev/null
  if [ $? -eq 0 ]
  then
     echo $dir 
  else
     echo "no s'ha creat el directori $dir" >> /dev/stderr
     status=$ERR_NODIR
  fi
done
exit $status

