#! /bin/bash
# isx25633105 ASIX-M01
# Març 2021
# 
# $ prog [-a file -b -c -d num -e ] arg[...]
# opcions
# arguments
# ------------------------------
ERR_NARGS=1
# 1) Validar argument:
if [ $# -lt 1 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 option arg"
  exit $ERR_NARGS
fi
opcions=""
arguments=""
file=""
num=""
while [ -n "$1" ]
do
  case $1 in
   -[bce])
     opcions="$opcions $1";;
   -a)
     file="$file $2"
     shift;;
   -d)
     min="$2"
     max="$3"
     shift
     shift;;
    *)
     arguments="$arguments $1";;
  esac
  shift
done
echo "File: $file"
echo "Min: $min"
echo "Max: $max"
echo "Opcions: $opcions"
echo "Arguments: $arguments"
exit 0


