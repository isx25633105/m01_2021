#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# 
# $ prog dir
# validar 1 arguments
# llistar el directori si existeix
# ------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) Validar argument:
if [ $# -ne 1 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# Validar si es un directori
mydir=$1
if ! [ -d $mydir ]
then
  echo "ERROR: $mydir no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi

ok=0

# XIXA
num=1
llista_elements=$(ls $mydir)
for fit in  $llista_elements
do
  echo "$num: $fit"
  ((num++))
done
exit 0

