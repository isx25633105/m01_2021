#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# 
# $ prog mes[...]
# validar 1 o més arguments
# per cada mes validar més [1,12]
# per cada mes diu el número de dies
# ------------------------------
ERR_NARGS=1
ERR_NORANG=2
# 1) Validar argument:
if [ $# -lt 1 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 notas[...]"
  exit $ERR_NARGS
fi
ok=0

# XIXA
for mes in $*
do
  if ! [ $mes -ge 1 -a $mes -le  12 ]; then
  echo "ERROR: #arg incorrecte" >> /dev/stderr
  echo "usage: $0 mes (valor de [1,12])" >> /dev/stderr
  ok=$ERR_NORANG
  fi
  case $mes in
    "1"|"3"|"5"|"7"|"8"|"10"|"12")
      echo "El $mes té 31 dies";;
    "4"|"6"|"9"|"11")
      echo "El $mes té 30 dies";;
    "2")
      echo "El $mes té 28 dies";;
  esac
done
exit $ok


