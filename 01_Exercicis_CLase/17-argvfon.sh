#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# 
# $ prog [-a -b -c -d -f -g ] arg[...]
# opcions
# arguments
# ------------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOFILE=3
# 1) Validar argument:
if [ $# -lt 1 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 file dir"
  exit $ERR_NARGS
fi
opcions=""
arguments=""
for arg in $*
do
  case $arg in
    "-a"|"-b"|"-c"|"-d"|"-f"|"-g")
      opcions="$opcions $arg";;
    *)
      arguments="$arguments $arg";;
  esac
done

echo "Opcions: $opcions"
echo "Arguments: $arguments"
exit 0


