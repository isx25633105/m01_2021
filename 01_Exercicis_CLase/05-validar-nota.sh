#! /bin/bash

# isx25633105 ASIX M01-ISO
# Febrer 2021
# Mostra si esà aprovat/suspès.
# validar rep un arg
# i que la nota és un valor vàlid 0-10
# ----------------------------------

# 1) Validem arguments
if [ $# -ne 1 ]
then
  echo "Error: numero arguments incorrecte"
  echo "Usage: $0 nota"
  exit 1
fi

# Validar nota pren un valor [0,10]
nota=$1
if ! [ $nota -ge 0 -a $nota -le 10 ]
then 
  echo "Error: nota no vàlida"
  echo "nota pren valors [0,10]"
  exit 2
fi

if [ $nota -lt 5 ]
then
  echo "Suspès"
else
  echo "Aprovat"
fi
exit 0
