#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# 
# $ prog file[...] dir
# validar x arguments
# per cada file
# validar file
# cp ---> desto
# copiar el fitxer al directori si existeix
# ------------------------------
ERR_NARGS=1
ERR_NODIR=2
ERR_NOFILE=3
# 1) Validar argument:
if [ $# -lt 2 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 file dir"
  exit $ERR_NARGS
fi

# Validar si es un directori i si es un fitxer
num_file=$(echo "$*" | cut -d' ' -f1-$(($#-1)))
mydir=$(echo "$*" | cut -d' ' -f$#)
OK=0
if ! [ -d $mydir ]
then
  echo "ERROR: $mydir no és un directori"
  echo "usage: $0 file dir"
  exit $ERR_NODIR
fi
#XIXA
for myfile in $num_file
do
  if ! [ -e $myfile ]
  then
    echo "ERROR: $myfile no és un fitxer"
    echo "usage: $0 file dir"
    ok=$ERR_NOFILE
  else
    cp $myfile $mydir
  fi
done
exit $OK


