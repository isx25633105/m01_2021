#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# Llistar el directori si existeix
# $ prog dir
# ------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) Validar argument:
if [ $# -ne 1 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# Validar existeix el directori
mydir=$1
if ! [ -d $mydir ]
then
  echo "ERROR: $mydir no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi
# 2) Xixa
ls $mydir
exit 0

