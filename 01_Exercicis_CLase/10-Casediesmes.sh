#! /bin/bash
# isx25633105 ASIX-M01
# Febrer 2021
# Mirar si és un més i comprobar si te 28,30,31
# $ prog dia
# ------------------------------
ERR_NARGS=1
ERR_NORANG=2
# 1) Validar argument:
if [ $# -ne 1 ]
then
  echo "ERROR: num arg incorrecte"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi


mes=$1
# 2) Validar mes:
if ! [ $mes -ge 1 -a $mes -le  12 ]; then
  echo "ERROR: #arg incorrecte"
  echo "usage: $0 mes (valor de [1,12])"
  exit $ERR_NORANG
fi

# XIXA
case $mes in
  "1"|"3"|"5"|"7"|"8"|"10"|"12")
    echo "El $mes té 31 dies";;
  "4"|"6"|"9"|"11")
    echo "El $mes té 30 dies";;
  "2")
    echo "El $mes té 28 dies";;
esac
exit 0


