#! /bin/bash
# Exemples de funcions

# validar rep 1 arg
# validar existeix usuari
# mostrar camp a camp
function usuari(){
  if [ $# -ne 1 ];
  then
    echo "ERROR: arg 1"
    echo "usage: $0 usuari"
    return 0
  fi
  usuari=$1
  grep "^$usuari:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ];
  then
    echo "L'usuari $usuari existeix al sistema"
  else
    echo "L'usuari $usuari no existeix al sistema" 
  fi
  return 0
}
function suma(){
  echo "La suma és: $(($1+$2))"
  return 0
}
function dia(){
 date
 return 0
}
