#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# 3) Processar arguments que són matricules:
# a) llistar les vàlides, del tipus: 9999-AAA.
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d̉errors (de no vàlides)

# ------------------------------
ERR_NARGS=1
STATUS=0
if [ $# -lt 1 ]
then
    echo "Error: nº arg incorrect"
    echo "Usage: $0 arg1[..]"
    exit $ERR_NARGS
fi

num=0
for matri in $*
do
 echo $matri | egrep "^[0-9]{4}-[A-Z]{3}$" &> /dev/null
 if  [ $? -eq 0 ]
 then
    echo "$matri"
 else
    echo "$matri" >> /dev/stderr
    ((num++))
    STATUS=$num
 fi
done
exit $STATUS
