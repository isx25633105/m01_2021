#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# Programa: prog -f|-d arg1 arg2 arg3 arg4
# a) Valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a dir, si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris.
# Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis.
# Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2.
# b) Ampliar amb el cas: prog -h|--help.

# ------------------------------
ERR_NARG=1
if  [ "$1" = "-h" -o "$1" = "--help" ]
then
  echo "Joan Almirall envia un correu a usuari@correu.com per solicitar ajuda"
  exit 0

elif [ $# -ne 5 ]
  then
  echo "ERROR numero args"
  echo "usage: $0 -f or -d arg1 arg2 arg3 arg4"
  exit $ERR_NARGS
fi
if [ "$1" != "-f" -a "$1" != "-d" ]
then
   echo "ERROR format arguments"
   echo "usage: $0 -f or -d arg1 arg2 arg3 arg4"
   exit $ERR_NARGS
fi

tipus=$1
shift
for arg in $*
do
  if ! [ $tipus "$arg" ]
  then
      status=2
  fi
done
exit $status

