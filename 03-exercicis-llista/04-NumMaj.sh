#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# 4) Processar stdin cmostrant per stdout les línies numerades i en majúscules.

# ------------------------------
num=0
maj=0
while read -r line
do
  maj=$(echo $line | tr "[a-z]" "[A-Z]")
  ((num++))
  echo "$num:$maj"
done

