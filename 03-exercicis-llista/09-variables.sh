#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# Programa: prog.sh [ -r -m -c cognom  -j  -e edat ]  arg…
#   Escriure el codi que desa en les variables: opcions, cognom, edat i arguments els valors corresponents.
#   No cal validar ni mostrar res!
#   Per exemple si es crida: $ prog.sh -e 18 -r -c puig -j wheel postgres ldap
#   retona:     opcions «-r -j», cognom «puig», edat «18», arguments «wheel postgres ldap»

#------------------------------------
ERR_NARGS=1
if [ $# -lt 1 ]
then
  echo "ERROR: $0 arg[...]"
  echo "usage: $0"
  exit $ERR_NARGS
fi
opcions=""
cognom=""
edat=""
arguments=""
for arg in $*
do
  case $arg in
    -[a-z])
      opcions="$opcions $arg";;
    "puig")
      cognom="$cognom $arg";;
    [0-9][0-9])
      edat="$edat $arg";;
    *) 
      arguments="$arguments $arg";;
  esac
done
echo "Opció:$opcions"
echo "Cognom:$cognom"
echo "Edat:$edat"
echo "Arguments:$arguments"

exit 0

    
