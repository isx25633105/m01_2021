#! /bin/bass
# isx25633105 ASIX-M01
# Marzo 2021
# 10) Programa: prog.sh
#    Rep per stdin GIDs i llista per stdout la informació de cada un d’aquests grups, en format: gname: GNAME, gid: GID, users: USERS
#
#--------------------------------
GREP=0
while read -r line
do
  GREP=$(egrep "^[^:]*:[^:]*:[^:]*:$line:" /etc/passwd)
  if [ $? -eq 0 ]
  then
    echo "gname:$(echo $GREP | cut -d: -f5)"
    echo "gid:$line"
    echo "user:$(echo $GREP | cut -d: -f1)"
  else
    echo "Aquest gid no es troba a la llista de usuaris"
  fi
done
