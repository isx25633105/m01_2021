#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# 8) Programa: prog file…
# a)Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s̉ha comprimit correctament, o un missatge d̉error per stderror si no s̉ha pogut comprimir. En finalitzar es mostra per stdout quants files ha comprimit.
# Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
# b) Ampliar amb el cas: prog -h|--help.

# ------------------------------
ERR_NARG=1
if  [ "$1" = "-h" -o "$1" = "--help" ]
then
  echo "Joan Almirall envia un correu a usuari@correu.com per solicitar ajuda"
  exit 0

elif [ $# -le 1 ]
then
  echo "ERROR numero args"
  echo "usage: $0 file...."
  exit $ERR_NARGS
fi

num=0
for arg in $*
do
  gzip $arg &> /dev/null
  if [ $? -eq 0 ]
  then 
    echo "Comprimit"
  else
    echo "No s'ha comprimit"
    ((num++))
  fi
done
exit $num

