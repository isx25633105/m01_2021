#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# Idem però rep els GIDs com a arguments de la línia d’ordres.

#-------------------------------------
ERR_NARGS=1
if [ $# -lt 1 ]
then
  echo "ERROR: $0 arg[...["
  echo "usage: $0"
  exit $ERR_NARGS
fi

GREP=0
STATUS=0
for gid in $*
do
  GREP=$(egrep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd)
  if [ $? -eq 0 ]
  then
    echo "gname:$(echo $GREP | cut -d: -f5)"
    echo "gid:$gid"
    echo "user:$(echo $GREP | cut -d: -f1)"
  else
    echo "El gid $gid no es troba al sistema"
    ((STATUS++))
  fi
done
exit $STATUS
  
