#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# 2) Processar els arguments i comptar quantes n̉hi ha de 3 o més caràcters.


# ------------------------------
ERR_NARGS=1
if [ $# -lt 1 ]
then
    echo "Error: nº arg incorrect"
    echo "Usage: $0 arg1[..]"
    exit $ERR_NARGS
fi

num=0
for arg in $*
do
 echo $arg | egrep '.{3,}' &> /dev/null
 if  [ $? -eq 0 ]
 then
    ((num++))
 fi
done
echo "Te $num arguments de 3 o més caracters"
exit 0
