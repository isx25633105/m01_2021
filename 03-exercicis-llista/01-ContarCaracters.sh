#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# Processar els arguments i mostrar per stdout només els de 4 o més caràcters.

# ------------------------------
ERR_NARGS=1
if [ $# -lt 1 ]
then
    echo "Error: nº arg incorrect"
    echo "Usage: $0 arg1[..]"
    exit $ERR_NARGS
fi


for arg in $*
do
 echo $arg | egrep '.{4,}'
done
exit 0
