#! /bin/bash
# isx25633105 ASIX-M01
# Marzo 2021
# 5) Processar stdin mostrant per stdout les línies de menys de 50 caràcters.

# ------------------------------
num=0
while read -r line
do
  num=$(echo $line | wc -c)
  if [ $num -lt 50 ]
  then
    echo "$line"
  fi
done

